import React from 'react';

const Burger = (props) => {

  let ingredients = props.ingredients;

  let ingNames = Object.keys(ingredients);

  const ingComponents = []; //

  ingNames.map((name) => { // 'salad'
    let amount = ingredients[name];

    for (let i = 0; i < amount; i++) {
      if (name === 'salad') {
        ingComponents.unshift(<div className="Salad" />);
      } else if (name === 'cheese') {
        ingComponents.unshift(<div className="Cheese" />);
      } else if (name === 'meat') {
        ingComponents.unshift(<div className="Meat" />);
      } else if (name === 'bacon') {
        ingComponents.unshift(<div className="Bacon" />);
      }
    }
  });


  return (
    <div className="Burger">
      <div className="BreadTop">
        <div className="Seeds1"></div>
        <div className="Seeds2"></div>
      </div>
      {ingComponents}
      <div className="BreadBottom"></div>
    </div>
  );
};

export default Burger;