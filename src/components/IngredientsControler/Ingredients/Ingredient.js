import React from 'react';
import './Ingredient.css';

const Ingredient = (props) => {
  let abc = props.ingredients
  console.log(abc)
  return (
    <div className='ingredient'>
      <span className='title'>{props.name}</span>
      <button onClick={props.less} disabled={props.ingredient === 0} className='less'>Less</button>
      <button onClick={props.more} className='more'>More</button>
    </div>
  );
};

export default Ingredient;
