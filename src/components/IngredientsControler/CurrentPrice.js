import React from 'react';

const CurrentPrice = (props) => {
  return (
    <div style={{marginBottom: '20px', borderBottom: '1px solid', paddingBottom: '15px'}}>
      Current Price: <span style={{fontWeight: 'bold'}}> {props.currPrice}</span>
    </div>
  );
};

export default CurrentPrice;