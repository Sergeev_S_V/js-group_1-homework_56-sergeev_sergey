import React from 'react';
import './IngredientsControler.css';
import CurrentPrice from "./CurrentPrice";
import Ingredient from "./Ingredients/Ingredient";

const IngredientsController = (props) => {
  console.log(props)
  return (
    <div className="ingredientsController">
      <CurrentPrice currPrice={props.currPrice}/>
      <Ingredient name='Salad'
                  less={() => props.less('salad')}
                  more={() => props.more('salad')}
                  ingredient={props.ingredients.salad}/>
      <Ingredient name='Bacon'
                  less={() => props.less('bacon')}
                  more={() => props.more('bacon')}
                  ingredient={props.ingredients.bacon}/>
      <Ingredient name='Cheese'
                  less={() => props.less('cheese')}
                  more={() => props.more('cheese')}
                  ingredient={props.ingredients.cheese}/>
      <Ingredient name='Meat'
                  less={() => props.less('meat')}
                  more={() => props.more('meat')}
                  ingredient={props.ingredients.meat}/>
    </div>
  );
};

export default IngredientsController;