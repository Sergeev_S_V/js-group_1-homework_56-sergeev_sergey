import React, { Component } from 'react';
import './BurgerBuilder.css';
import Burger from "../components/Burger/Burger";
import IngredientsController from "../components/IngredientsControler/IngredientsControler";

class BurgerBuilder extends Component {

  state = {
    ingredients: {
      salad: 0,
      meat: 0,
      cheese: 0,
      bacon: 0
    },
    totalCost: 20
  };

  price = {
    basePrice: 20,
    salad: 5,
    meat: 50,
    cheese: 20,
    bacon: 30
  };

  moreIngredients = (currIngredient) => {
    let ingredients = {...this.state.ingredients};
    ingredients[currIngredient]++;
    let totalCost = this.state.totalCost;
    totalCost += this.price[currIngredient];

    this.setState({ingredients, totalCost});
  };

  lessIngredients = (currIngredient) => {
    let ingredients = {...this.state.ingredients};
    ingredients[currIngredient] > 0 ? ingredients[currIngredient]-- : null;
    let totalCost = this.state.totalCost;
    if (totalCost > this.price.basePrice) {
      const changedPrice = totalCost - this.price[currIngredient];
      if (changedPrice >= 20) totalCost -= this.price[currIngredient];
    }

    this.setState({ingredients, totalCost});
  };

  autoIncreasingPrice = () => {

  };


  render () {


    return (
      <div style={{display: 'flex', marginTop: '50px'}}>
        <Burger ingredients={this.state.ingredients}/>
        <IngredientsController currPrice={this.state.totalCost}
                               less={this.lessIngredients}
                               more={this.moreIngredients}
                               ingredients={this.state.ingredients}
        />
      </div>
    );
  }
}

export default BurgerBuilder;